<?php

abstract class Vivant implements Evolution
{
    public $nom;
    public $taxonomie;
    public $masse;

    public function mourir()
    {
        return "Je suis mort";
    }

    public function Croitre($x)
    {
        return "Je grandis de $x centimetres";
    }
}